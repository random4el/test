const Koa = require('koa');
const Router = require('koa-router');
const koaBody = require('koa-body');
const books = require('./db/books')

const app = new Koa();

app.use(koaBody())
app.use(async (ctx, next) => {
    ctx.set('Content-Type', 'application/json');
    await next()
})
app.listen(3000).on('listening', () => {
    console.log('Listen on port 3000')
})


const router = new Router();

router
    .get('/', (ctx, next) => {
        ctx.body = `
            Available routes:
            GET     /books (?autor &date &limit &offset &order['id', 'date', 'autor'])
            GET     /books/:id
            POST    /books ({...fields})
            PUT     /books/:id  ({...fields})
            `;
    })
    .get('/books', async (ctx, next) => {
        const {autor, date, limit, offset, order} = ctx.query
        const options = {}
        options.filter = {}
        if (autor) options.filter.autor = autor
        if (date) options.filter.date = date

        Object.assign(options, {limit, offset, order})

        ctx.body = await books.selectBooks(options);
    })
    .get('/books/:id', async (ctx, next) => {
        const {id} = ctx.params

        ctx.body = await books.selectBooks({filter: {id}});
    })
    .post('/books', async (ctx, next) => {
        const {
            title,
            date,
            autor,
            description,
            image,
        } = ctx.request.body

        await books.insertBooks([title, date, autor, description, image]);

        ctx.body = {status: 'OK'}
    })
    .put('/books/:id', async (ctx, next) => {
        const {id} = ctx.params
        const {
            title,
            date,
            autor,
            description,
            image,
        } = ctx.request.body

        await books.updateBooks({title, date, autor, description, image}, {id: parseInt(id)});
        ctx.body = {status: 'OK'}
    })


app
    .use(router.routes())
    .use(router.allowedMethods());