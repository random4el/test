const {promiseQuery, connection} = require('../')
const {TABLE_NAME} = require('../utils/constants')

const NAMES = [
    "Nick Rivera",
    "Dallas Villarreal",
    "Lia Sanchez",
    "Martin Webster",
    "Madden Hale",
    "Kameron Norton",
    "Issac Underwood",
    "Ace Erickson",
    "Asher Joseph",
    "Shawn Lucero",
    "Esperanza Brennan",
    "Jermaine Alvarez"
]

function randomString(){
    return Math.random().toString(36).substring(2)
}

function toSqlStr(val) {
    return `'${val}'`
}

function randomItem() {
    const title = randomString();
    const description = `${randomString()} ${randomString()} ${randomString()}`;
    const date = new Date(Math.floor(Math.random() * new Date())).toISOString().substr(0,10)
    const autor = NAMES[Math.floor(Math.random() * NAMES.length)]

    return [
        toSqlStr(title),
        toSqlStr(date),
        toSqlStr(autor),
        toSqlStr(description),
        'NULL'
    ]
}

function fill(count) {
    const res = []
    for (let i=count;i>0;i--){
        res.push(randomItem())
    }

    const resStr = res.map(e=>(`(${e.join()})`)).join()

    return promiseQuery(`INSERT INTO ${TABLE_NAME} (title, \`date\`, autor, description, image) VALUES ${resStr}`)

}

Promise.resolve().then(async()=>{
    await fill(1e5)
    connection.end();
})