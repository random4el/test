const {connection, promiseQuery} = require('../')
const {TABLE_NAME} = require('../utils/constants')

function createBooks() {
    return promiseQuery(`
    CREATE TABLE IF NOT EXISTS \`${TABLE_NAME}\`(
        \`id\` INTEGER NOT NULL auto_increment ,
        \`title\` VARCHAR(255) NOT NULL,
        \`date\` DATE,
        \`autor\` VARCHAR(255) NOT NULL,
        \`description\` VARCHAR(255),
        \`image\` LONGBLOB,
    PRIMARY KEY (\`id\`)
    ) ENGINE=InnoDB;`
    )
}

async function createIndex() {
    await promiseQuery(`
        CREATE INDEX books_date on \`${TABLE_NAME}\`(\`date\`);
    `)
    await promiseQuery(`
        CREATE INDEX books_autor on \`${TABLE_NAME}\`(\`autor\`);
    `)
}

Promise.resolve().then(async () => {
    await createBooks()
    await createIndex()
    connection.end();
})

