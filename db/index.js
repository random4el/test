const mysql = require('mysql')
const {promisify} = require('util')
const config = require('../config')

const connection = mysql.createConnection({
    host: config.DB_HOST,
    user: config.DB_USER,
    password: config.DB_PASS,
    database: config.DB_NAME
})

const promiseQuery = promisify(connection.query).bind(connection)

module.exports = {connection, promiseQuery}