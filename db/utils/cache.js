const LRU = require("lru-cache")

const options = {
    max: 1000000,
    length: function (n) {
        return Array.isArray(n) ? n.length : 1
    }
    , maxAge: 1000 * 60
}
const cache = new LRU(options)

async function getAndSet(key, func){
    let res = this.get(key)
    if(!res){
        res = await func()
        this.set(key, res)
    }
    return res
}

Object.assign(cache, {getAndSet})

module.exports = cache