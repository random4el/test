const _ = require('lodash')
const esc = require('sqlstring').escape

const CONDITION_MAP = {
    $gt: '>',
    $gte: '>=',
    $lt: '<',
    $lte: '<=',
    $ne: '!='
}

module.exports = {
    buildQuery(sql, where = {}, orderBy = {}, limit = null, offset = null) {
        function parser(value, fieldname) {
            if (typeof value === 'undefined') {
                return null
            }
            if (value instanceof Array) {
                return value.map(val => parser(val, fieldname))
            }
            if (_.isObject(value)) {
                return _.map(value, (v, k) => {
                    switch (k) {
                        case '$raw':
                            return `${fieldname} ${v}`
                        case '$gt':
                        case '$gte':
                        case '$lt':
                        case '$lte':
                        case '$ne': {
                            return `${fieldname} ${CONDITION_MAP[k]} ${esc(v)}`
                        }
                        case '$in': {
                            return '(' + v.map(val => parser(val, fieldname)).join(' or ') + ')'
                        }
                    }
                })
            }
            return `${fieldname} = ${esc(value)}`
        }

        where = _.chain(where)
            .map(parser)
            .flattenDeep()
            .compact()
            .value()
            .join(' and ')

        return sql
            .replace('@where', _.size(where) ? `where ${where}` : '')
            .replace('@orderBy', _.size(orderBy) ? `order by ${orderBy}` : '')
            .replace('@limit', !!limit ? `limit ${limit}` : '')
            .replace('@offset', !!offset ? `offset ${offset}` : '')
            .trim()
    },
    $raw($raw) {
        return {$raw}
    },
    $gt($gt) {
        return {$gt}
    },
    $gte($gte) {
        return {$gte}
    },
    $lt($lt) {
        return {$lt}
    },
    $lte($lte) {
        return {$lte}
    },
    $ne($ne) {
        return {$ne}
    },
    $in($in) {
        return {$in}
    }
}
