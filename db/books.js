const _ = require('lodash')
const {connection, promiseQuery} = require('./')
const qb = require('./utils/query-builder')
const cache = require('./utils/cache')
const {TABLE_NAME} = require('./utils/constants')


module.exports = {
    selectBooks({filter, order = null, limit = null, offset = null}) {
        const key = [_.values(filter).sort().join("_"), "o_", order ? order : "", "l_", limit ? limit : "", "of", offset ? offset : ""].join('_')

        return cache.getAndSet(key, () => {
            let query = qb.buildQuery(`
                    SELECT * FROM ${TABLE_NAME}
                    @where
                    @orderBy
                    @limit
                    @offset
                `, filter, order, limit, offset)

            return promiseQuery(query)
        })
    },
    insertBooks([title, date = null, autor, description = null, image = null]) {
        return promiseQuery(`
                INSERT INTO ${TABLE_NAME} (
                    title,
                    date,
                    autor,
                    description,
                    image
                ) VALUES (?,?,?,?,?)`, [title, date, autor, description, image])
    },
    updateBooks(values, {id}) {
        const updateString = _.keys(values).reduce((acc, key) => {
            if (!!values[key]) acc.push(`${key}=${connection.escape(values[key])}`)
            return acc
        }, []).join()

        if (!!updateString) {
            const query = qb.buildQuery(`
                UPDATE ${TABLE_NAME}
                SET ${updateString}
                @where
    `, {id})

            return promiseQuery(query)
        }
    }
}